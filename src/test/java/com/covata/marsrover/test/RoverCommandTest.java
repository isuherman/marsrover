package com.covata.marsrover.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.shell.Bootstrap;
import org.springframework.shell.core.CommandResult;
import org.springframework.shell.core.JLineShellComponent;

public class RoverCommandTest 
{
    @Test
    public void testCommand() 
    {
	Bootstrap bootstrap = new Bootstrap();

	JLineShellComponent shell = bootstrap.getJLineShellComponent();

	CommandResult cr = shell.executeCommand("plateu-size 5 5");
	assertEquals(true, cr.isSuccess());

	cr = shell.executeCommand("select-rover 1");
	assertEquals(true, cr.isSuccess());

	cr = shell.executeCommand("rover-position 1 2 N");
	assertEquals(true, cr.isSuccess());

	cr = shell.executeCommand("rover-explore LMLMLMLMM");
	assertEquals(true, cr.isSuccess());

	cr = shell.executeCommand("select-rover 2");
	assertEquals(true, cr.isSuccess());

	cr = shell.executeCommand("rover-position 3 3 E");
	assertEquals(true, cr.isSuccess());

	cr = shell.executeCommand("rover-explore MMRMMRMRRM");
	assertEquals(true, cr.isSuccess());

	cr = shell.executeCommand("get-position-output 1");
	assertEquals("Rover 1 position: 1 3 N", cr.getResult());

	cr = shell.executeCommand("get-position-output 2");
	assertEquals("Rover 2 position: 5 1 E", cr.getResult());
    }
}
