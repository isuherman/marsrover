package com.covata.marsrover.services.impl;

import org.springframework.stereotype.Service;

import com.covata.marsrover.services.PlateuService;
import com.covata.marsrover.services.RoverService;

@Service
public class RoverServiceImpl implements RoverService
{
    private String roverHeading = "N";
    private Integer roverNumber;
    private Integer[] position = {0, 0};
    private PlateuService plateuService;
    
    @Override
    public void setPlateuService(PlateuService plateuService)
    {
        this.plateuService = plateuService;
    }

    @Override
    public String getRoverHeading()
    {
	return roverHeading;
    }

    @Override
    public void setRoverHeading(String roverHeading)
    {
	this.roverHeading = roverHeading;
    }

    @Override
    public Integer getRoverXPosition()
    {
	return position[0];
    }

    @Override
    public void setRoverXPosition(Integer roverXPosition)
    {
	if (position[0]+roverXPosition > plateuService.getXSize())
	{
	    throw new RuntimeException("X movement is larger than maximum X plateu size.");
	}
	
	position[0] += roverXPosition;
    }

    @Override
    public Integer getRoverYPosition()
    {
	return position[1];
    }

    @Override
    public void setRoverYPosition(Integer roverYPosition)
    {
	if (position[1]+roverYPosition > plateuService.getYSize())
	{
	    throw new RuntimeException("Y movement is larger than maximum Y plateu size.");
	}
	
	position[1] += roverYPosition;
    }

    @Override
    public RoverService newInstance()
    {
	return new RoverServiceImpl();
    }

    @Override
    public void setRoverNumber(Integer roverNumber)
    {
	this.roverNumber = roverNumber;	
    }    

    @Override
    public Integer getRoverNumber()
    {
	return roverNumber;
    }

    @Override
    public String getPosition()
    {
	return getRoverXPosition()+" "+getRoverYPosition()+" "+getRoverHeading();
    }

    @Override
    public void setExplors(String exploration)
    {
	char[] explors = exploration.toCharArray();
	
	for (char explor : explors)
	{
	    switch (explor)
	    {
	    	case 'L': 
	    	switch(roverHeading)
	    	{
	    		case "N":
	    		roverHeading = "W";
	    		break;

	    		case "W":
	    		roverHeading = "S";
	    		break;

	    		case "S":
	    		roverHeading = "E";
	    		break;

	    		case "E":
	    		roverHeading = "N";
	    		break;
	    	}
	    	break;
	    	
	    	case 'R': 
		switch(roverHeading)
		{
		    	case "N":
		    	roverHeading = "E";
		    	break;

		    	case "W":
		    	roverHeading = "N";
		    	break;

		    	case "S":
		    	roverHeading = "W";
		    	break;

		    	case "E":
		    	roverHeading = "S";
		    	break;
		}
		break;
		
	    	case 'M':
	    	switch(roverHeading)
		{
		    	case "N":
		    	setRoverYPosition(1);
		    	break;

		    	case "W":
			setRoverXPosition(-1);
		    	break;

		    	case "S":
			setRoverYPosition(-1);
		    	break;

		    	case "E":
			setRoverXPosition(1);
		    	break;
		}
	    	break;
	    
	    	default:
		throw new RuntimeException("Invalid heading: "+explor);
	    }
	}
    }
}
