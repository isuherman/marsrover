package com.covata.marsrover.services.impl;

import org.springframework.stereotype.Service;

import com.covata.marsrover.services.PlateuService;

@Service
public class PlateuServiceImpl implements PlateuService
{
    private Integer xSize;
    private Integer ySize;

    @Override
    public Integer getXSize()
    {
	return xSize;
    }
    
    @Override
    public void setXSize(Integer xSize)
    {
	this.xSize = xSize;
    }

    @Override
    public Integer getYSize()
    {
	return ySize;
    }

    @Override
    public void setYSize(Integer ySize)
    {
	this.ySize = ySize;
    }

    @Override
    public String getPlateuSize()
    {
	return xSize+"x"+ySize;
    }
}
