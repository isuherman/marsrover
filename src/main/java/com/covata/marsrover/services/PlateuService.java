package com.covata.marsrover.services;

public interface PlateuService
{
    public Integer getXSize();
    public void setXSize(Integer xSize);
    public Integer getYSize();
    public void setYSize(Integer ySize);
    public String getPlateuSize();
}
