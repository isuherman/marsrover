package com.covata.marsrover.services;

public interface RoverService
{
    public String getRoverHeading();
    public Integer getRoverXPosition();
    public RoverService newInstance();
    public void setRoverHeading(String commands);
    public void setRoverXPosition(Integer roverXPosition);
    public Integer getRoverYPosition();
    public void setRoverYPosition(Integer roverYPosition);
    public void setRoverNumber(Integer i);
    public Integer getRoverNumber();
    public String getPosition();
    public void setExplors(String exploration);
    public void setPlateuService(PlateuService plateuService);
}
