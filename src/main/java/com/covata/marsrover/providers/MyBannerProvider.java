package com.covata.marsrover.providers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.support.DefaultBannerProvider;
import org.springframework.shell.support.util.OsUtils;
import org.springframework.stereotype.Component;

import com.covata.marsrover.Main;

/**
 * @author Suherman
 *
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MyBannerProvider extends DefaultBannerProvider  
{
    private Package mainPackage = Main.class.getPackage();

    @Value("#{properties['application.name']}")
    private String applicationName;

    public String getBanner() 
    {
	StringBuffer buf = new StringBuffer();
	buf.append("==========================="+OsUtils.LINE_SEPARATOR);
	buf.append(applicationName+OsUtils.LINE_SEPARATOR);
	buf.append("Version: " + this.getVersion()+OsUtils.LINE_SEPARATOR);
	buf.append("===========================");

	return buf.toString();
    }

    public String getVersion() 
    {
	return mainPackage.getImplementationVersion();
    }

    @Override
    public String getWelcomeMessage() 
    {
	return "Please enter command to execute "+applicationName+". Press TAB to see all available commands.";
    }

    @Override
    public String getProviderName() 
    {
	return mainPackage.getName();
    }
}