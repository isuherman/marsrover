package com.covata.marsrover.models;

public class Command
{
    private Integer commandNumber;
    private String commandName;
    private String command;

    public Integer getCommandNumber()
    {
        return commandNumber;
    }

    public void setCommandNumber(Integer commandNumber)
    {
        this.commandNumber = commandNumber;
    }

    public String getCommandName()
    {
        return commandName;
    }

    public void setCommandName(String commandName)
    {
        this.commandName = commandName;
    }

    public String getCommand()
    {
        return command;
    }

    public void setCommand(String command)
    {
        this.command = command;
    }
}
