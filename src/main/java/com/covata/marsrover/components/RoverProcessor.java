package com.covata.marsrover.components;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import com.covata.marsrover.services.PlateuService;
import com.covata.marsrover.services.RoverService;

@Component
public class RoverProcessor implements CommandMarker 
{
    @Autowired
    private PlateuService plateuService;

    @Autowired
    private RoverService roverService;

    @Value("#{properties['total.rovers']}")
    private String totalRovers;

    private List<RoverService> rovers;
    private RoverService selectedRover;
    
    private final Log logger = LogFactory.getLog(RoverProcessor.class);
    
    @PostConstruct
    public void init() throws Exception
    {
	if (totalRovers != null)
	{
	    totalRovers(totalRovers);
	}
    }
    
    @CliCommand(value = "plateu-size", help = "Input plateu size")
    public void plateuSize(@CliOption(key="", mandatory=true) final String message) throws Exception
    {
	String[] commands = validateCommand(message, 2);

	plateuService.setXSize(Integer.parseInt(commands[0]));
	plateuService.setYSize(Integer.parseInt(commands[1]));

	receiveCommand(message, "plateu size="+plateuService.getPlateuSize());
    }

    @CliCommand(value = "total-rovers", help = "Set total rovers")
    public void totalRovers(@CliOption(key="", mandatory=true) final String message)
    {
	validateCommand(message, 1);
	Integer total = Integer.parseInt(message);

	rovers = new ArrayList<RoverService>();
	for (int i=0; i<total; i++)
	{
	    RoverService rover = roverService.newInstance();
	    rover.setRoverNumber(i+1);
	    rover.setPlateuService(plateuService);
	    rovers.add(rover);
	}
    }

    @CliCommand(value = "select-rover", help = "Select active rover")
    public void selectRover(@CliOption(key="", mandatory=true) final Integer selected)
    {
	selectedRover = rovers.get(selected-1);
    }

    @CliCommand(value = "rover-position", help = "Set rover position")
    public void roverPosition(@CliOption(key="", mandatory=true) final String message)
    {
	String[] commands = validateCommand(message, 3);
	
	selectedRover.setRoverXPosition(Integer.parseInt(commands[0]));
	selectedRover.setRoverYPosition(Integer.parseInt(commands[1]));
	selectedRover.setRoverHeading(commands[2]);

	receiveCommand(message, "rover number "+selectedRover.getRoverNumber()+" position now is: "+selectedRover.getPosition());
    }

    @CliCommand(value = "rover-explore", help = "Set rover exploration")
    public void roverExplore(@CliOption(key="", mandatory=true) final String message)
    {
	validateCommand(message, 1);
	selectedRover.setExplors(message);
	
	receiveCommand(message, "rover number "+selectedRover.getRoverNumber()+" position now is: "+selectedRover.getPosition());
    }

    @CliCommand(value = "execute-queues", help = "Execute rover queue commands.")
    public void executeQueues(@CliOption(key="", mandatory=true) final String message)
    {
	validateCommand(message, 1);
	selectedRover.setExplors(message);
	
	receiveCommand(message, "rover number "+selectedRover.getRoverNumber()+" position now is: "+selectedRover.getPosition());
    }

    @CliCommand(value = "get-position", help = "Execute rover queue commands.")
    public void getPosition(@CliOption(key="", mandatory=true) final Integer roverNumber)
    {
	logger.info(getPositionOutput(roverNumber));
    }
    

    @CliCommand(value = "get-position-output", help = "Execute rover queue commands.")
    public String getPositionOutput(@CliOption(key="", mandatory=true) final Integer roverNumber)
    {
	selectRover(roverNumber);
	return "Rover "+roverNumber+" position: "+selectedRover.getPosition();
    }

    @CliCommand(value = "summary", help = "Show last position of all rovers.")
    public void summary()
    {
	for (RoverService rover : rovers)
	{
	    getPosition(rover.getRoverNumber());
	}
    }

    
    private String[] validateCommand(String message, Integer length)
    {
	String[] commands = message.split(" ");

	if (commands.length != length)
	{
	    String error = "Invalid command, please provide exactly "+length+" characters command separated by space.";
	    if (length == 1)
	    {
		error = "Invalid command, please provide exactly "+length+" character command only.";
	    }

	    throw new RuntimeException(error);
	}

	return commands;
    }

    private void receiveCommand(String command, String result)
    {
	logger.info("command received: "+command+", result: "+result);
    }
}
