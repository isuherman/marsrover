package com.covata.marsrover;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.shell.Bootstrap;
import org.springframework.shell.core.JLineShellComponent;

import com.covata.marsrover.components.RoverProcessor;
import com.covata.marsrover.models.Command;
import com.covata.marsrover.models.Queue;
import com.covata.marsrover.utilities.RoverQueue;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Driver class to run the mars-rover project. 
 * 
 * @author Suherman
 *
 */
public class Main 
{
    private final static Log logger = LogFactory.getLog(RoverProcessor.class);

    /**
     * Main class that delegates to Spring Shell's Bootstrap class in order to simplify debugging inside an IDE
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception 
    {
	if (args.length == 0)
	{
	    Bootstrap.main(args);

	}
	else
	{
	    Bootstrap bootstrap = new Bootstrap();	
	    JLineShellComponent shell = bootstrap.getJLineShellComponent();

	    List<Command> commands = new ObjectMapper().readValue(readFile(args[0]), new TypeReference<List<Command>>(){});
	    
	    RoverQueue roverQueue = new RoverQueue();
	    for (Command command : commands)
	    {
		roverQueue.addQueue(command.getCommandNumber(), command.getCommandName(), command.getCommand());
	    }

	    for (Map.Entry<Integer, List<Queue>> entry : roverQueue.getQueue().entrySet()) 
	    {
		Integer key = entry.getKey();

		if (key > 0)
		{
		    shell.executeScriptLine("select-rover "+key);
		}

		for (Queue queue : entry.getValue())
		{
		    shell.executeScriptLine(queue.getCommand()+" "+queue.getMessage());
		}
	    }

	    shell.executeScriptLine("summary");
	    roverQueue.resetQueue();
	}
    }

    private static String readFile(String filePath)
    {
	logger.info("Reading queue file: "+filePath);
	try
	{
	    InputStream in = Main.class.getClassLoader().getResourceAsStream(filePath);
	    return inputStreamToString(in);
	}
	catch (Exception e)
	{
	    logger.error("Caught exception: "+e.getMessage());
	}
	
	return null;
    }

    private static String inputStreamToString(InputStream inputStream)
    {
	StringWriter writer = new StringWriter();
	try
	{
	    IOUtils.copy(inputStream, writer, "UTF-8");
	}
	catch (IOException e)
	{
	    e.printStackTrace();
	}

	return writer.toString();
    }
}
