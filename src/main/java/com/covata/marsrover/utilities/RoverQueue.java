package com.covata.marsrover.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.covata.marsrover.models.Queue;

public class RoverQueue
{
    private Map<Integer, List<Queue>> roverQueues = new HashMap<Integer, List<Queue>>();
    
    public void addQueue(Integer roverNumber, String command, String message) throws Exception
    {
	List<Queue> queues = roverQueues.get(roverNumber);
	
	if (queues == null)
	{
	    queues = new ArrayList<Queue>();
	}
	
	Queue queue = new Queue();
	queue.setCommand(command);
	queue.setMessage(message);
	
	queues.add(queue);
	
	roverQueues.put(roverNumber, queues);
    }
    
    public Map<Integer, List<Queue>> getQueue()
    {
	return roverQueues;
    }
    
    public void resetQueue()
    {
	roverQueues = new HashMap<Integer,List<Queue>>();
    }
}
