Instruction:
------------

First, build the project:
  $>mvn package
  
There are two ways of executing this project:

1. Executing by pre-defined movement input from file: mars-rover/src/main/resources/input.json:

  $>java -jar target/mars-rover-1.0.0.jar input.json
  
2. Executing by run spring shell:
  $>java -jar target/mars-rover-1.0.0.jar
  
On the second input, one should manually input the command to rover by entering the valid input commands. Press TAB for help.
Some example valid commands scenario:

- plateu-size 5 5
- select-rover 1
- rover-position 1 2 N
- rover-explore LMLMLMLMM
- select-rover 2
- rover-position 3 3 E
- rover-explore MMRMMRMRRM
- summary


Code Explanation:
-----------------

The code is using Spring Shell framework, in which the org.springframework.shell.Bootstrap is become the entry point of the application. 
Upon initiated by Main.java the bootstrap will be executing some commands that defined by @CliCommand annotation inside RoverProcessor.java class's methods. 
RoverProcessor class injects two external services (Plateu and Rover services) and perform lazy self initialization of Rover's services depends on the number of available rover that defined in the mars-rover.properties file.
The rover's movement is immediately executed if its processed directly from command execution of Spring Shell. However, some batch process of command execution is possible while its queued by adding input.json parameter while executing the main jar file from the command line (1st way above).
With this extra parameter, Main.java class will map the input.json file into set of Command.java's DTO objects. By the special utility method inside RoverQueue.java class, the queues were sorted based on the Rover's number. However, number 0 indicated a generic command such as plateu size adjustment.
During compilation by maven, the RoverCommandTest.java class test unit will be executed and perform exactly same commands as this test intended to and will evaluate the result to be match with this test's expected answer to maintain code consistency.
At the end of the process, the entire execution activities were stored inside log/activity.out log file for archiving purpose and all executed commands were stored inside the default spring-shell.log log file.


Assumptions:
------------
1. Total number of rover are two (which defined in mars-rover/src/main/resources/mars-rover.properties file)
2. While entering command (manual or automatic) one should define to which rover the command is intended to (with select-rover command) or else the system will get confused to determine the input movement
3. Manual input via command line is necessary to manually override rover movement if required (refer to The Martian movie which eventually is able to safe a human life)
4. Rover can't move outside plateu size; however, if its necessary, plateu size can always be altered any time from the command line or input.json file
5. Error exception's that produced by invalid class casting, invalid rover commands, movement and operations will be displayed as RunTime exception which will not cause application to stop working. Instead the exception's error message will be displayed as error message by the Spring Shell's framework.
6. Invalid input values will be ignored and skipped, however the next input should still be processed.
7. The main Rover movement logic resides in the RoverServiceImpl.java polymorphism implementation class which deriving from its main RoverService.java interface. The idea is for later expansion if the rover movement algorithm changed.  
8. Text based input and process will be more suitable for this code since the distance of Earth and Mars will be 20 minutes lag. By minimizing unnecessary interface (such as web) will reduce the amount of data communication between Mars and Houston.